<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="bg-purple-900 text-white">
        <div class="container mx-auto px-4">
            <div class="grid grid-cols-3 my-10">
                @foreach ($posts as $post)
                    <div class="bg-purple-800 hover:bg-purple-700 border border-blue-300 p-5 rounded-md m-2">
                        <h2 class="font-bold text-lg mb-4">{{ $post->title }}</h2>
                        <p class="text-xs">{{ $post->excerpt }}</p>
                        <p class="text-xs text-right"> {{ $post->published_at }}</p>
                    </div>
                @endforeach
            </div>
            <div class="mb-10">
                {{ $posts->links() }}
            </div>
        </div>
    </body>
</html>
