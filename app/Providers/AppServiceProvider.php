<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Pagination\Paginator;  // Se importa la clase Paginator para que funcione la paginación de la vista index.blade.php con tailwind

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); // !Siempre agregar para evitar errores
        Paginator::defaultView('vendor.pagination.tailwind'); // !Se agrega para que funcione tailwind en la paginación
        // !El comando para agregar es php artisan vendor:publish --tag=tailwind
        // !Esto crea una carpeta la siguente ruta: resources/views/vendor/pagination/tailwind.blade.php
    }
}
