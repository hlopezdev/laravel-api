<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\PostController as PostControllerV1; 
use App\Http\Controllers\Api\V2\PostController as PostControllerV2;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Api\LoginController;

// V1
Route::apiResource('v1/posts', PostControllerV1::class)->only(['index', 'show', 'store', 'update', 'destroy'])->middleware('auth:sanctum');

// V2
Route::apiResource('v2/posts', PostControllerV2::class)->only(['index', 'show', 'store', 'update', 'destroy']);

Route::post('login', [LoginController::class, 'login']);
